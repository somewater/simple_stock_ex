package db

import model.Account
import org.scalatest.{Matchers, WordSpec}

class AccountsTest extends WordSpec with Matchers {
  "Accounts" should {
    "correctly handle multiple clients" in {
      val accounts: Accounts = new AccountsMemoryImpl
      accounts.add(Account("C1", Map("A1" -> 5, "A2" -> 6)))
      accounts.add(Account("C2", Map("A1" -> 7, "A3" -> 8)))

      accounts.change("C1", "A1", 3)
      accounts.change("C2", "A3", -3)

      accounts.all() should have size(2)
      val c1 = accounts.all().find(_.id == "C1").get
      val c2 = accounts.all().find(_.id == "C2").get
      c1.instruments shouldEqual Map("A1" -> 8, "A2" -> 6)
      c2.instruments shouldEqual Map("A1" -> 7, "A3" -> 5)
    }

    "store client with zero balance" in {
      val accounts: Accounts = new AccountsMemoryImpl
      accounts.add(Account("C1", Map.empty))
      accounts.all() should have size(1)
      val c1 = accounts.all().toList.head
      c1.id shouldEqual "C1"
      c1.instruments shouldBe empty
    }

    "store client after zeroing out of balance" in {
      val accounts: Accounts = new AccountsMemoryImpl
      accounts.add(Account("C1", Map("A1" -> 5)))
      accounts.change("C1", "A1", -5)
      accounts.all() should have size(1)
      val c1 = accounts.all().toList.head
      c1.id shouldEqual "C1"
      c1.instruments shouldBe empty
    }

    "remember about debt" in {
      val accounts: Accounts = new AccountsMemoryImpl
      accounts.add(Account("C1", Map("A1" -> 5)))
      accounts.change("C1", "A1", -10)
      accounts.all() should have size(1)
      val c1 = accounts.all().toList.head
      c1.instruments shouldEqual Map("A1" -> -5)
    }
  }
}
