package stockex

import db.Accounts
import model.{Account, Order}
import org.mockito.Matchers
import org.scalatest.WordSpec
import org.scalatest.mockito.MockitoSugar
import org.mockito.Mockito._
import org.mockito.Matchers._


class StockExTest extends WordSpec with MockitoSugar {
  private def ?[T]: T = any[T]()

  abstract class Fixture {
    val accounts = mock[Accounts]
    val stockEx: StockEx = new StockExImpl(accounts)
    val account1 = Account("C1", Map("$" -> 100, "A1" -> 1, "A2" -> 2))
    val account2 = Account("C2", Map("$" -> 200, "A1" -> 1, "A3" -> 3))

    doNothing().when(accounts).add(?)
  }

  "StockEx" should {
    "don't executre orders partially" in new Fixture {
      stockEx.addOrder(Order("C1", "A1", Order.Buy, 100, 2))
      stockEx.addOrder(Order("C2", "A1", Order.Sell, 50, 2))
      stockEx.addOrder(Order("C3", "A1", Order.Sell, 200, 2))

      verify(accounts, never()).change(?, ?, ?)
    }

    "don't match orders with non equal price" in new Fixture {
      stockEx.addOrder(Order("C1", "A1", Order.Buy, 100, 2))
      stockEx.addOrder(Order("C2", "A1", Order.Sell, 100, 1))

      verify(accounts, never()).change(?, ?, ?)
    }

    "match appropriate orders" in new Fixture {
      doNothing().when(accounts).change(?, ?, ?)

      stockEx.addOrder(Order("C1", "A1", Order.Buy, 100, 1))
      stockEx.addOrder(Order("C1", "A2", Order.Buy, 100, 1))
      stockEx.addOrder(Order("C1", "A4", Order.Buy, 100, 1))
      stockEx.addOrder(Order("C1", "A5", Order.Sell, 100, 1))

      stockEx.addOrder(Order("C2", "A1", Order.Sell, 100, 1))
      stockEx.addOrder(Order("C2", "A3", Order.Sell, 100, 1))
      stockEx.addOrder(Order("C2", "A4", Order.Buy, 100, 1))
      stockEx.addOrder(Order("C2", "A5", Order.Sell, 100, 1))

      verify(accounts).change("C1", "A1", 1)
      verify(accounts).change("C2", "A1", -1)
      verify(accounts).change("C1", "$", -100)
      verify(accounts).change("C2", "$", 100)

      verify(accounts, never()).change(?, Matchers.eq("A2"), ?)
      verify(accounts, never()).change(?, Matchers.eq("A3"), ?)
      verify(accounts, never()).change(?, Matchers.eq("A4"), ?)
      verify(accounts, never()).change(?, Matchers.eq("A5"), ?)
    }

    "don't match orders from one client" in new Fixture {
      stockEx.addOrder(Order("C1", "A1", Order.Buy, 100, 2))
      stockEx.addOrder(Order("C1", "A1", Order.Sell, 100, 1))

      verify(accounts, never()).change(?, ?, ?)
    }

    "correctly apply new balance" in new Fixture {
      doNothing().when(accounts).change(?, ?, ?)

      stockEx.addOrder(Order("C1", "A1", Order.Buy, 100, 7))
      stockEx.addOrder(Order("C2", "A1", Order.Sell, 100, 7))

      verify(accounts).change("C1", "A1", 7)
      verify(accounts).change("C2", "A1", -7)
      verify(accounts).change("C1", "$", -700)
      verify(accounts).change("C2", "$", 700)
    }

    "don't check negative balance" in new Fixture {
      doNothing().when(accounts).change(?, ?, ?)

      stockEx.addAccount(Account("C1", Map("$" -> 1, "A1" -> 1)))
      stockEx.addAccount(Account("C2", Map("$" -> 1, "A1" -> 1)))
      stockEx.addOrder(Order("C1", "A1", Order.Buy, 100, 100))
      stockEx.addOrder(Order("C2", "A1", Order.Sell, 100, 100))

      verify(accounts).change("C1", "A1", 100)
      verify(accounts).change("C2", "A1", -100)
      verify(accounts).change("C1", "$", -10000)
      verify(accounts).change("C2", "$", 10000)
    }
  }
}
