package util

import java.io.PrintWriter

import model.{Account, Order}

import scala.io.Source

/**
  * Чтение/запись данных в файл
  */
class FileOps {
  private val Instruments = List("$", "A", "B", "C", "D")

  def readAccounts(filepath: String): Iterator[Account] = {
    Source.fromFile(filepath).getLines().map { line =>
      val cols = line.split("\t")
      val instruments = Instruments.zip(cols.drop(1).map(_.toLong)).toMap
      Account(cols(0), instruments)
    }
  }

  def writeAccounts(filepath: String, accounts: Iterator[Account]): Unit = {
    val w = new PrintWriter(filepath)
    try {
      for (account <- accounts) {
        val cols = account.id :: Instruments.map(i => account.instruments.getOrElse(i, 0).toString)
        w.write(cols.mkString("\t") + "\n")
      }
    } finally {
      w.close()
    }
  }

  def readOrders(filepath: String): Iterator[Order] = {
    Source.fromFile(filepath).getLines().map { line =>
      val cols = line.split("\t")
      val clientId = cols(0)
      val action = if (cols(1) == "b") Order.Buy else Order.Sell
      val instrument = cols(2)
      val price = cols(3).toLong
      val quantity = cols(4).toLong
      Order(cols(0), instrument, action, price, quantity)
    }
  }
}
