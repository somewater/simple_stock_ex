import java.nio.file.Paths

import db.{Accounts, AccountsMemoryImpl}
import stockex.{StockEx, StockExImpl}
import util.FileOps

object Main {
  def main(args: Array[String]): Unit = {
    var clientsFilepath = Paths.get("data", "clients.txt").toString
    var ordersFilepath = Paths.get("data", "orders.txt").toString
    var resultFilepath = Paths.get("data", "result.txt").toString
    args.sliding(2, 2).toList.collect {
      case Array("--clients", filepath: String) => clientsFilepath = filepath
      case Array("--orders", filepath: String) => ordersFilepath = filepath
      case Array("--result", filepath: String) => resultFilepath = filepath
    }

    val fileOps = new FileOps()
    val accounts: Accounts = new AccountsMemoryImpl
    val stockEx: StockEx = new StockExImpl(accounts)

    fileOps.readAccounts(clientsFilepath).foreach(stockEx.addAccount)
    fileOps.readOrders(ordersFilepath).foreach(stockEx.addOrder)
    fileOps.writeAccounts(resultFilepath, stockEx.accounts())
  }
}
