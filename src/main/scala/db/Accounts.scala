package db

import model.Account

/**
  * Интерфейс работы с аккаунтами клиентов
  */
trait Accounts {
  def add(account: Account): Unit

  def change(clientId: String, instrument: String, delta: Long): Unit

  def all(): Iterator[Account]
}
