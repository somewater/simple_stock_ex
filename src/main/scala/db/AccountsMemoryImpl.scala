package db
import model.Account

class AccountsMemoryImpl extends Accounts {
  type Key = (String, String) // client, instrument
  private var accounts = Map.empty[Key, Long].withDefaultValue(0L)

  def add(account: Account): Unit = {
    if (account.instruments.isEmpty) {
      change(account.id, "$", 0)
    } else {
      account.instruments.foreach { case (instrument, value) =>
        change(account.id, instrument, value)
      }
    }
  }

  override def change(clientId: String, instrument: String, delta: Long): Unit = {
    val key = (clientId, instrument)
    accounts =  accounts.updated(key, accounts(key) + delta)
  }

  override def all(): Iterator[Account] = {
    accounts
      .groupBy { case ((clientId, instrument), _) => clientId }
      .toSeq.sortBy(_._1)
      .map { case (clientId, entries) =>
        val instruments = entries.collect { case ((clientId, instrument), value) if value != 0 => instrument -> value }
        Account(clientId, instruments)
      }.toIterator
  }
}
