package stockex

import model.{Account, Order}

/**
  * Интерфейс биржи
  */
trait StockEx {
  def addAccount(account: Account): Unit

  def addOrder(order: Order): Unit

  def openOrders(): Iterator[Order]

  def accounts(): Iterator[Account]
}
