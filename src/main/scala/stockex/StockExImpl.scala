package stockex

import db.Accounts
import model.{Account, Order}
import model.Order.Action

import scala.annotation.tailrec
import scala.collection.immutable.Queue

/**
  * Реализация сервиса стоков, используя сервис БД для хранения балансов по аккаунтам
  */
class StockExImpl(db: Accounts) extends StockEx {
  private type OrderKey = (String, Order.Action, Long, Long) // instrument, action, price, quantity
  private var orders = Map.empty[OrderKey, Queue[Order]].withDefaultValue(Queue.empty[Order])

  override def addAccount(account: Account): Unit = db.add(account)

  override def addOrder(order: Order): Unit = {
    val orderKey = (order.instrument, order.action, order.price, order.quantity)
    val oppositeOrderKey = (order.instrument, order.action.opposite(), order.price, order.quantity)

    orders.get(oppositeOrderKey) match {
      case Some(oppositeOrders) =>
        findAnotherClientOrder(order.clientId, oppositeOrders) match {
          case Some((oppositeOrder, newQueue)) =>
            transaction(order, oppositeOrder)
            if (newQueue.isEmpty) {
              orders = orders - oppositeOrderKey
            } else {
              orders = orders.updated(oppositeOrderKey, newQueue)
            }
          case None =>
            orders = orders.updated(orderKey, orders(orderKey).enqueue(order))
        }
      case _ =>
        orders = orders.updated(orderKey, orders(orderKey).enqueue(order))
    }
  }

  override def openOrders(): Iterator[Order] = orders.values.flatten.toIterator

  override def accounts(): Iterator[Account] = db.all()

  /**
    * Поиск ближайшего по порядку очереди заказа, выставленного НЕ указанным клиентом
    */
  @tailrec private def findAnotherClientOrder(clientId: String,
                                              queue: Queue[Order],
                                              skippedElems: List[Order] = Nil): Option[(Order, Queue[Order])] = {
    if (queue.isEmpty) {
      None
    } else {
      val (elem, tail) = queue.dequeue
      if (elem.clientId != clientId) {
        var newQueue = tail
        skippedElems.foreach(elem => newQueue = elem +: newQueue)
        Some(elem -> newQueue)
      } else {
        findAnotherClientOrder(clientId, tail, elem :: skippedElems)
      }
    }
  }

  private def transaction(order1: Order, order2: Order): Unit = {
    def applyOrder(order: Order) = {
      val coef = if (order.action == Order.Buy) 1 else -1
      db.change(order.clientId, order.instrument, order.quantity * coef)
      db.change(order.clientId, Account.USD, -coef * order.price * order.quantity)
    }
    applyOrder(order1)
    applyOrder(order2)
  }
}
