package model

/**
  * Модель для представления баланса клиента
  */
case class Account(id: String, instruments: Map[String, Long])

object Account {
  val USD = "$"
}