package model

import model.Order.Action

/**
  * Заявка клиента на покупку/продажу ценной бумаги
  */
case class Order(clientId: String, instrument: String, action: Action, price: Long, quantity: Long) {
  assert(price > 0)
  assert(quantity > 0)
}

object Order {
  sealed trait Action {
    def opposite(): Action
  }
  case object Sell extends Action {
    override def opposite(): Action = Buy
  }
  case object Buy extends Action {
    override def opposite(): Action = Sell
  }
}